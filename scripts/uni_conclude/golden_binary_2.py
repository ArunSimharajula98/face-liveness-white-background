import keras
from keras.models import Sequential
from keras.models import load_model
from keras.models import Model
from PIL import Image
from keras import backend as K
K.set_image_dim_ordering('tf')
import numpy as np
from keras.applications import mobilenet
from keras import models
from keras import layers
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
import os
import time
from keras.preprocessing import image
from keras.applications.imagenet_utils import preprocess_input
from keras.applications.imagenet_utils import decode_predictions
from keras.layers import Dense, Activation, Flatten, Dropout
from keras.layers import merge, Input
from keras.utils import np_utils
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
import json
import argparse
import sys

fortrain = sys.argv[1]  # prints python_script.py
fortest = sys.argv[2]  # prints var1
outing = sys.argv[3]
hyparameter = sys.argv[4]
weightfile = sys.argv[5]

#jsonfile = "11/hyperparameters.json"
#weightfile = "11/technical_rename.hdf5"

with open(hyparameter) as json_file:  
    hp = json.load(json_file)


backend = None

import os
import argparse
#import dlib
import cv2
#import imutils
	
print("bilinear_resize")

harness = str(fortest) + "/" + "Real"
ss = 0 

for fl in os.listdir(harness):
    #print(fl)
    if ss <= 5:
        if fl == ".DS_Store" or fl == "_DS_Store":
            #print(fl)
            print("stupid files")
        else:
            images2 = os.path.join(harness,fl)
            print(fl)
            img = cv2.imread(images2)
            height=img.shape[0]
            weight=img.shape[1]
            print(height, weight)
            ss = ss + 1
    else:
        print("done")
        break




def _depthwise_conv_block(inputs, pointwise_conv_filters, alpha,
                          depth_multiplier=1, strides=(1, 1), block_id=1):
    """Adds a depthwise convolution block.
    A depthwise convolution block consists of a depthwise conv,
    batch normalization, relu6, pointwise convolution,
    batch normalization and relu6 activation.
    # Arguments
        inputs: Input tensor of shape `(rows, cols, channels)`
            (with `channels_last` data format) or
            (channels, rows, cols) (with `channels_first` data format).
        pointwise_conv_filters: Integer, the dimensionality of the output space
            (i.e. the number of output filters in the pointwise convolution).
        alpha: controls the width of the network.
            - If `alpha` < 1.0, proportionally decreases the number
                of filters in each layer.
            - If `alpha` > 1.0, proportionally increases the number
                of filters in each layer.
            - If `alpha` = 1, default number of filters from the paper
                 are used at each layer.
        depth_multiplier: The number of depthwise convolution output channels
            for each input channel.
            The total number of depthwise convolution output
            channels will be equal to `filters_in * depth_multiplier`.
        strides: An integer or tuple/list of 2 integers,
            specifying the strides of the convolution
            along the width and height.
            Can be a single integer to specify the same value for
            all spatial dimensions.
            Specifying any stride value != 1 is incompatible with specifying
            any `dilation_rate` value != 1.
        block_id: Integer, a unique identification designating
            the block number.
    # Input shape
        4D tensor with shape:
        `(batch, channels, rows, cols)` if data_format='channels_first'
        or 4D tensor with shape:
        `(batch, rows, cols, channels)` if data_format='channels_last'.
    # Output shape
        4D tensor with shape:
        `(batch, filters, new_rows, new_cols)`
        if data_format='channels_first'
        or 4D tensor with shape:
        `(batch, new_rows, new_cols, filters)`
        if data_format='channels_last'.
        `rows` and `cols` values might have changed due to stride.
    # Returns
        Output tensor of block.
    """
    channel_axis = -1
    pointwise_conv_filters = int(pointwise_conv_filters * alpha)

    if strides == (1, 1):
        x = inputs
    else:
        x = layers.ZeroPadding2D(((0, 1), (0, 1)),
                                 name='conv_pad_%d' % block_id)(inputs)
    x = layers.DepthwiseConv2D((3, 3),
                               padding='same' if strides == (1, 1) else 'valid',
                               depth_multiplier=depth_multiplier,
                               strides=strides,
                               use_bias=False,
                               name='conv_dw_%d' % block_id)(x)
    x = layers.BatchNormalization(
        axis=channel_axis, name='conv_dw_%d_bn' % block_id)(x)
    x = layers.ReLU(6., name='conv_dw_%d_relu' % block_id)(x)

    x = layers.Conv2D(pointwise_conv_filters, (1, 1),
                      padding='same',
                      use_bias=False,
                      strides=(1, 1),
                      name='conv_pw_%d' % block_id)(x)
    x = layers.BatchNormalization(axis=channel_axis,
                                  name='conv_pw_%d_bn' % block_id)(x)
    return layers.ReLU(6., name='conv_pw_%d_relu' % block_id)(x)



# If imagenet weights are being loaded, input must have a static square shape (one of (128, 128), (160, 160), (192, 192), or (224, 224))
model_base = mobilenet.MobileNet(include_top=False, input_shape=(224,224, 3),alpha=1)

#for layer in model_base.layers:
#   layer.trainable = True

#for layer in model_base.layers:
#    print(layer, layer.trainable)
# model_base.load_weights('technical.hdf5')

output=model_base.output
# Add any other layers you want to `output` here...

output=Dense(10,activation='relu')(output)
model = Model(model_base.input, output)
model.summary()
model.load_weights(weightfile)
for layer in model_base.layers:
    layer.trainable = True
 
# Create the model
#--model1 = models.Sequential() 
# Add the vgg convolutional base model
#--model1.add(model)
#--model1.summary()
#layer_name22 = model1.layers[-1]
#model2= Model(inputs=model1.input, outputs=model1.get_layer(layer_name22).output)

#model1.layers.pop()
#model1.layers.pop()
#output2 = model1.output
#model1.output = [model1.layers[-1].output]
#model1.layers.pop()
#output2 = model.get_output_at(3)

#model2 = K.function([model.layers[0].input],
                                  #[model.layers[54].output])
#layer_output = get_3rd_layer_output([x])[0]


output2=model.get_layer("conv_pw_13_relu").output
#output2=DepthSepConv(kernel=[3, 3], stride=2, depth=2048)(output2)
#output2=separable_conv2d_same(output2, kernel_size=[3, 3], stride=2, depth=2048)
output2=_depthwise_conv_block(output2, 2048, 1, 1, strides=(2, 2), block_id=56)
keras.layers.normalization.BatchNormalization(epsilon=1e-06, mode=0, momentum=0.9, weights=None)
output2=Activation('relu')(output2)

#output2=_depthwise_conv_block(output2, 2048, 1, 1, strides=(1, 1), block_id=57)
#output2=separable_conv2d_same(output2, kernel_size=[3, 3], stride=2, depth=2048)
#output2=DepthSepConv(kernel=[3, 3], stride=1, depth=2048)(output2)
#keras.layers.normalization.BatchNormalization(epsilon=1e-06, mode=0, momentum=0.9, weights=None)
#output2=Activation('relu')(output2)


model2 = Model(model.input, output2)
model2.summary()
model23 = models.Sequential()
model23.add(model2)
model23.summary()

#model23.add(DepthSepConv(kernel=[3, 3], stride=2, depth=2048))
#keras.layers.normalization.BatchNormalization(epsilon=1e-06, mode=0, momentum=0.9, weights=None)
#model23.add(Activation('relu')

#model23.add(DepthSepConv(kernel=[3, 3], stride=1, depth=2048))
#keras.layers.normalization.BatchNormalization(epsilon=1e-06, mode=0, momentum=0.9, weights=None)
#model23.add(Activation('relu')
# Add new layers
model23.add(layers.Flatten())
#model1.add(layers.Dense(12,activation='relu'))
#model1.add(layers.Dropout(0.5))
model23.add(layers.Dense(12, activation='relu'))
model23.add(layers.Dense(8, activation='relu'))
model23.add(layers.Dense(5, activation='relu'))
#model1.add(layers.Dropout(0.5))
model23.add(layers.Dense(1,activation='sigmoid'))
 
# Show a summary of the model. Check the number of trainable parameters
model23.summary()

train_datagen = ImageDataGenerator(
      rescale=1./255,
      horizontal_flip=True,
      vertical_flip=True)
 
validation_datagen = ImageDataGenerator(rescale=1./255)
 

train_generator = train_datagen.flow_from_directory(
        fortrain,
        target_size=(224, 224),
        batch_size=hp['batch_size'],
        class_mode='binary')
 
validation_generator = validation_datagen.flow_from_directory(
        fortest,
        target_size=(224, 224),
        batch_size=hp['batch_size'],
        class_mode='binary')
#sgd = optimizers.SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True)
#checkpoint = ModelCheckpoint('model-{epoch:03d}.h5', verbose=1, monitor='val_loss',save_best_only=True, mode='auto')  
#early_stopping_callback = EarlyStopping(monitor='val_loss', patience=5)
#csv_logger=keras.callbacks.CSVLogger('training.log', separator=',', append=False)
# Compile the model
model23.compile(loss=hp['loss'],
              optimizer=hp['optimizer'],
              metrics=['accuracy'])


# Train the model
model23.fit_generator(
      train_generator,
      steps_per_epoch=hp['steps_per_epoch'],
      epochs=hp['epochs'],
      validation_data=validation_generator,
      validation_steps=hp['validation_steps'])
 

#hist = model1.fit(X_train, y_train, batch_size=256, epochs=12, verbose=1, validation_data=(X_test, y_test))

#print('Training time: %s' % (t - time.time()))
#(loss, accuracy) = model1.evaluate(X_test, y_test, batch_size=10, verbose=1)

#print("[INFO] loss={:.4f}, accuracy: {:.4f}%".format(loss,accuracy * 100))

# Save the model
model23.save(outing)

